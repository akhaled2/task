import './App.css';
import React from 'react'
import Script from "react-inline-script"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="App">

                <div className="topbar  container-fluid">
                    <header className="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="header">
                        <div className="col-xs-12 top_header">
                            <div className="row">
                                <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <ul className="left">
                                        <li><span>Wed Dec 16 2020</span></li>
                                        <li><span className="temperature"><span className=""> Cairo </span><img
                                            src="static/cloud.svg"
                                            alt=""/><span>18</span></span>
                                        </li>
                                        <li><a href="download.html"> <span>Download App<i className="fa fa-mobile"
                                                                                          aria-hidden="true"></i></span>
                                        </a></li>
                                        <li className="contribute">
                                            <div className="contDiv">
                                                <div className="first">
                                                    <button
                                                        className="sec"
                                                        tabIndex="0" type="button">
                                                        <div>
                                                            <div
                                                                className="thired">
                              <span
                                  className="four">Contribute</span>
                                                            </div>
                                                        </div>
                                                    </button>
                                                </div>
                                                <img src="static/file.svg" alt=""/>
                                                <div
                                                    className="five"></div>
                                            </div>
                                        </li>
                                        <li className="search-section">
                                            <div className="top_search"><a href="#" className="search_btn"><i
                                                className="fa fa-search"></i></a>
                                                <div id="search_box" className="">
                                                    <div
                                                        className="six">
                                                        <div
                                                            className="seven">
                                                            Search ...
                                                        </div>
                                                        <input type="text"
                                                               className="eight"
                                                               value="" id="search_input"/>
                                                        <div>
                                                            <hr aria-hidden="true"
                                                                style={{
                                                                    borderTop: 'none',
                                                                    borderLeft: 'none',
                                                                    borderRight: 'none',
                                                                    borderBottomStyle: 'solid',
                                                                    borderBottomWidth: '1px',
                                                                    borderColor: '#e0e0e0',
                                                                    bottom: '8px',
                                                                    boxSizing: 'content-box',
                                                                    position: 'absolute',
                                                                    width: '100%',
                                                                    mozBoxSizing: 'content-box'
                                                                }}/>
                                                            <hr aria-hidden="true"
                                                                style={{
                                                                    borderTop: 'none',
                                                                    borderLeft: 'none',
                                                                    borderRight: 'none',
                                                                    borderBottomStyle: 'solid',
                                                                    borderBottomWidth: '2px',
                                                                    borderColor: '#00bcd4',
                                                                    bottom: '8px',
                                                                    boxSizing: 'content-box',
                                                                    margin: 0,
                                                                    position: 'absolute',
                                                                    width: '100%',
                                                                    transform: 'scaleX(0)',
                                                                    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                                    mozBoxSizing: 'content-box',
                                                                    webkitTransform: 'scaleX(0)',
                                                                    msTransform: 'scaleX(0)',
                                                                    webkitTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                                    mozTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
                                                                }}/>
                                                        </div>
                                                    </div>
                                                    <a href="#" className="go_btn">Go</a></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div className="right">
                                        <div className="contribute">
                                            <div className="contDiv">
                                                <div style={{
                                                    color: 'rgba(0, 0, 0, 0.87)',
                                                    backgroundColor: '#ffffff',
                                                    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                    boxSizing: 'border-box',
                                                    fontFamily: 'Roboto, sans-serif',
                                                    webkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
                                                    boxShadow: '0 1px 6px rgba(0, 0, 0, 0.12),0 1px 4px rgba(0, 0, 0, 0.12)',
                                                    borderRadius: '2px',
                                                    display: 'inline-block',
                                                    minWidth: '88px',
                                                    webkitTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                    mozTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                    mozBoxSizing: 'border-box'
                                                }}>
                                                    <button
                                                        style={{
                                                            border: '10px',
                                                            boxSizing: 'border-box',
                                                            display: 'inline-block',
                                                            fontFamily: 'Roboto, sans-serif',
                                                            webkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
                                                            cursor: 'pointer',
                                                            'textDecoration': 'none',
                                                            margin: '0',
                                                            padding: '0',
                                                            outline: 'none',
                                                            fontSize: 'inherit',
                                                            fontWeight: 'inherit',
                                                            position: 'relative',
                                                            height: '36px',
                                                            lineHeight: '36px',
                                                            width: '100%',
                                                            borderRadius: '2px',
                                                            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                            backgroundColor: '#ffffff',
                                                            textAlign: 'center',
                                                            mozBoxSizing: 'border-box',
                                                            webkitTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                            mozTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
                                                        }}
                                                        tabIndex="0" type="button">
                                                        <div>
                                                            <div
                                                                style={{
                                                                    height: '36px',
                                                                    borderRadius: '2px',
                                                                    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                                    top: '0',
                                                                    webkitTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                                                    mozTransition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
                                                                }}>
                                                        <span
                                                            style={{
                                                                position: 'relative',
                                                                opacity: '1',
                                                                fontSize: '14px',
                                                                letterSpacing: '0',
                                                                textTransform: 'uppercase',
                                                                fontWeight: 500,
                                                                margin: '0',
                                                                userSelect: 'none',
                                                                paddingLeft: '16px',
                                                                paddingRight: '16px',
                                                                color: 'rgba(0, 0, 0, 0.87)',
                                                                webkitUserSelect: 'none',
                                                                mozUserSelect: 'none',
                                                                msUserSelect: 'none'
                                                            }}>Contribute</span>
                                                            </div>
                                                        </div>
                                                    </button>
                                                </div>
                                                <img src="static/file.svg" alt=""/>
                                                <div
                                                    style={{
                                                        position: 'fixed',
                                                        left: '50%',
                                                        display: '-webkit-box',
                                                        bottom: '0',
                                                        zIndex: '2900',
                                                        visibility: 'hidden'
                                                    }}></div>
                                            </div>
                                        </div>
                                        <ul className="social_media">
                                            <li><a href="https: //www.facebook.com/NileFM" target="_blank"><i
                                                className="fa fa-facebook"
                                                aria-hidden="true"></i></a>
                                            </li>
                                            <li><a href="https: //twitter.com/NileFM" target="_blank"><i
                                                className="fa fa-twitter"
                                                aria-hidden="true"></i></a>
                                            </li>
                                            <li><a href="https: //www.instagram.com/nilefmonline/" target="_blank"><i
                                                className="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="https: //www.youtube.com/user/NileFM" target="_blank"><i
                                                className="fa fa-youtube" aria-hidden="true"></i></a></li>
                                            <li><a href="https: //www.mixcloud.com/nilefm/" target="_blank"><img
                                                src="static/mixCloud.png" className="mix" alt=""/></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 mob-header 'relative'">
                                <div className="logo">
                                    <div><a href="index.html"><img src="static/logo.png" alt="logo"/></a><a
                                        href="watch-live.html"
                                        className="hide watch-mob"><img
                                        src="static/watch-mobile.png" className="for-s-mobile" alt=""/><img
                                        src="static/watch-mobile.png" className="for-mobile-tablet" alt=""/></a></div>
                                    <a className="menu_btn"><i className="fa fa-bars" aria-hidden="true"></i><i
                                        className="fa fa-times"
                                        aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div className="leaderboard"></div>
                            </div>
                        </div>
                        <div className="main_menu" id="main_menu">
                            <div className="row">
                                <ul>
                                    <li className="home"><a href="index.html">Home</a></li>
                                    <li className="shows"><a href="shows.html"> Shows </a></li>
                                    <li className="presenters"><a href="presenters.html"> Presenters </a></li>
                                    <li className="schedule"><a href="schedule.html"> Schedule </a></li>
                                    <li className="videos"><a href="videos.html"> Videos </a></li>
                                </ul>
                            </div>
                        </div>
                    </header>
                    <div className="col-lg-12 col-xs-12 fixed_bar ">
                        <div className="row">
                            <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12 breaking_news">
                                <div><span> Spotlight </span>
                                    <div className="slick-slider slick-initialized">
                                        <div className="slick-list">
                                            <div className="slick-track" style={{width: '0%', left: 'NaN%'}}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" col-lg-1 col-md-1 col-sm-2 col-xs-2 wtach_live ">
                                <div><a href=" watch-live.html" target=" _blank"><img src="
                                             static/watch_live.png" alt=""/><img
                                    src=" static/watch_live2.gif" alt=""/></a></div>
                            </div>
                            <div className=" col-lg-4 col-md-4 col-sm-10 col-xs-10 on_air">
                                <div className="">
                                    <div className=" row">
                                        <div className=" col-lg-3 col-md-3 col-sm-2 col-xs-2">
                                            <div className=" on_air_img">
                                                <div
                                                    style={{
                                                        width: '100%',
                                                        height: '70px',
                                                        backgroundImage: 'url(static/showDefault.jpg)',
                                                        backgroundRepeat: 'no-repeat',
                                                        backgroundPosition: 'center',
                                                        backgroundSize: 'cover'
                                                    }}></div>
                                                <img src="static/showDefault.jpg" alt=" Non-Stop Music"/></div>
                                        </div>
                                        <div className=" col-lg-6 col-md-6 col-sm-7 col-xs-7 no_padding">
                                            <div><h6>On Air Now</h6><h5>Non-Stop Music</h5></div>
                                            <div><span><i className=" fa fa-play-circle" aria-hidden=" true"></i><div
                                                style={{overflow: 'hidden'}}><span></span></div>
                                    </span>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 no_padding">
                                            <div><a href="listen-live.html" target="_blank" className="btn"><i
                                                className="fa fa-caret-right"
                                                aria-hidden="true"></i>
                                                Listen</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main_body">
                        <div>
                            <section className="home_featured_body">
                                <div className="home_featured entertainm"><a
                                    title="New Mobile App &quot,Mat5afeesh&quot, Offers A Wide Range Of High-Tech Features To Combat Sexual Harassment In Egypt"
                                    href="digest/article/7210/new-mobile-app-mat5afeesh-offers-a-wide-range-of-high-tech-features-to-combat-sexual-harassment-in-egypt.html">
                                    <div className="feat_img">
                                        <div className="feat_details"><h5> Digest</h5>
                                            <div>
                                                <h2>
                                                    <a href="digest/article/7210/new-mobile-app-mat5afeesh-offers-a-wide-range-of-high-tech-features-to-combat-sexual-harassment-in-egypt.html">
                                                        <div style={{overflow: 'hidden'}}>
                                                            < span> New Mobile App &quot,Mat5afeesh&quot, Offers A Wide Range Of High-Tech Features To Combat Sexual Harassment In Egypt</span>
                                                        </div>
                                                    </a></h2>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div
                                                style={{
                                                    width: '100%',
                                                    height: '240px',
                                                    backgroundImage: 'url(http://storage.googleapis.com/nrpassets/uploads/articles/1607861212-BeFunky-collage_-_2020-12-13T140651.012.jpg)',
                                                    backgroundRepeat: 'no-repeat',
                                                    backgroundPosition: 'center',
                                                    backgroundSize: 'cover'
                                                }}></div>
                                            <img
                                                src="http://storage.googleapis.com/nrpassets/uploads/articles/1607861212-BeFunky-collage_-_2020-12-13T140651.012.jpg"
                                                className="for-s-mobile" alt=""/></div>
                                    </div>
                                </a></div>
                                <div className="home_featured entertainm"><a
                                    title="El-Sisi Promises That COVID-19 Vaccines Will Be Given Out To Egyptians Free Of Charge"
                                    href="digest/article/7206/el-sisi-promises-that-covid-19-vaccine-will-be-given-out-free-of-charge.html">
                                    <div className="feat_img">
                                        <div className="feat_details"><h5> Digest</h5>
                                            <div>
                                                <h2>
                                                    <a href="digest/article/7206/el-sisi-promises-that-covid-19-vaccine-will-be-given-out-free-of-charge.html">
                                                        <div style={{overflow: 'hidden'}}><span>El-Sisi Promises That COVID-19 Vaccines Will Be Given Out To Egyptians Free Of Charge</span>
                                                        </div>
                                                    </a></h2>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div
                                                style={{
                                                    width: '100%',
                                                    height: '240px',
                                                    backgroundImage: 'url(http://storage.googleapis.com/nrpassets/uploads/articles/1607853183-BeFunky-collage_-_2020-12-13T114943.648.jpg)',
                                                    backgroundRepeat: 'no-repeat',
                                                    backgroundPosition: 'center',
                                                    backgroundSize: 'cover'
                                                }}></div>
                                            <img
                                                src="http://storage.googleapis.com/nrpassets/uploads/articles/1607853183-BeFunky-collage_-_2020-12-13T114943.648.jpg"
                                                className="for-s-mobile" alt=""/></div>
                                    </div>
                                </a></div>
                                <div className="home_featured entertainm"><a
                                    title="Egypt Issues Commemorative Coins In Celebration Of Islamic Cairo&#x27,s 1050th Anniversary"
                                    href="digest/article/7194/egypt-issues-commemorative-coins-in-celebration-of-islamic-cairo-s-1050th-anniversary.html">
                                    <div className="feat_img">
                                        <div className="feat_details"><h5> Digest</h5>
                                            <div>
                                                <h2>
                                                    <a href="digest/article/7194/egypt-issues-commemorative-coins-in-celebration-of-islamic-cairo-s-1050th-anniversary.html">
                                                        <div style={{overflow: 'hidden'}}><span>Egypt Issues Commemorative Coins In Celebration Of Islamic Cairo&#x27,s 1050th Anniversary</span>
                                                        </div>
                                                    </a></h2>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div
                                                style={{
                                                    width: '100%',
                                                    height: '240px',
                                                    backgroundImage: 'url(http://storage.googleapis.com/nrpassets/uploads/articles/1607586068-BeFunky-collage_-_2020-12-10T094022.196.jpg)',
                                                    backgroundRepeat: 'no-repeat',
                                                    backgroundPosition: 'center',
                                                    backgroundSize: 'cover'
                                                }}></div>
                                            <img
                                                src="http://storage.googleapis.com/nrpassets/uploads/articles/1607586068-BeFunky-collage_-_2020-12-10T094022.196.jpg"
                                                className="for-s-mobile" alt=""/></div>
                                    </div>
                                </a></div>
                                <div className="home_featured life_style"><a title=" "
                                                                             href="life/article/7186/egypt-launches-caricature-exhibit-of-nobel-laureates-naguib-mahfouz-gabriel-garcia-marquez.html">
                                    <div className="feat_img">
                                        <div className="feat_details"><h5> Life</h5>
                                            <div>
                                                <h2>
                                                    <a href="life/article/7186/egypt-launches-caricature-exhibit-of-nobel-laureates-naguib-mahfouz-gabriel-garcia-marquez.html">
                                                        <div style={{overflow: 'hidden'}}><span> </span></div>
                                                    </a></h2>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div
                                                style={{
                                                    width: '100%',
                                                    height: '240px',
                                                    backgroundImage: 'url(http://storage.googleapis.com/nrpassets/uploads/articles/1607505344-BeFunky-collage_-_2020-12-09T111434.914.jpg)',
                                                    backgroundRepeat: 'no-repeat',
                                                    backgroundPosition: 'center',
                                                    backgroundSize: 'cover'
                                                }}></div>
                                            <img
                                                src="http://storage.googleapis.com/nrpassets/uploads/articles/1607505344-BeFunky-collage_-_2020-12-09T111434.914.jpg"
                                                className="for-s-mobile" alt=""/></div>
                                    </div>
                                </a></div>
                                <div className="home_featured entertainm"><a
                                    title="Egypt&#x27,s Health Ministry Announces New, More Effective COVID-19 Medical Protocol"
                                    href="digest/article/7182/egypt-s-health-ministry-announces-new-more-effective-covid-19-medical-protocol.html">
                                    <div className="feat_img">
                                        <div className="feat_details"><h5> Digest</h5>
                                            <div>
                                                <h2>
                                                    <a href="digest/article/7182/egypt-s-health-ministry-announces-new-more-effective-covid-19-medical-protocol.html">
                                                        <div style={{overflow: 'hidden'}}><span>Egypt&#x27,s Health Ministry Announces New, More Effective COVID-19 Medical Protocol</span>
                                                        </div>
                                                    </a></h2>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div
                                                style={{
                                                    width: '100%',
                                                    height: '240px',
                                                    backgroundImage: 'url(http://storage.googleapis.com/nrpassets/uploads/articles/1607496983-BeFunky-collage_-_2020-12-09T085405.512.jpg)',
                                                    backgroundRepeat: 'no-repeat',
                                                    backgroundPosition: 'center',
                                                    backgroundSize: 'cover'
                                                }}></div>
                                            <img
                                                src="http://storage.googleapis.com/nrpassets/uploads/articles/1607496983-BeFunky-collage_-_2020-12-09T085405.512.jpg"
                                                className="for-s-mobile" alt=""/></div>
                                    </div>
                                </a></div>
                            </section>
                        </div>
                        <div className="main_body">
                            <div className="row">
                                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline news"><a
                                                href="/beats"><span>Beats</span></a><strong><a href="/beats">View
                                                More</a></strong></h2>
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                    <div className="no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/beats/article/7228/zayn-malik-teases-his-return-to-the-recording-studio">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608031454-1_3.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608031454-1_3.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/beats/article/7228/zayn-malik-teases-his-return-to-the-recording-studio">Fans
                                                                        Are Losing It As Zayn Malik Teases His Return To
                                                                        The Recording Studio</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 15, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>Former One Direction singer Zayn Malik is freaking his fans out as he teased a picture from inside a recording studio, hinting that he’s working on new music soon.
Things are… </span></div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/beats/article/7226/shawn-mendes-becomes-the-youngest-male-artist-to-top-billboard-200-chart-with-four-full-length-studio-albums">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608027933-1_2.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608027933-1_2.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/beats/article/7226/shawn-mendes-becomes-the-youngest-male-artist-to-top-billboard-200-chart-with-four-full-length-studio-albums">Shawn
                                                                        Mendes Becomes The Youngest Male Artist To Top
                                                                        Billboard 200 Chart With Four Full-Length Studio
                                                                        Albums</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 15, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>There’s no denying that singer Shawn Mendes is a rising star to be reckoned with, and with his latest album “Wonder” he’s become the youngest male artist to top the… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/beats/article/7220/mariah-carey-s-all-i-want-for-christmas-is-you-tops-the-uk-chart-for-the-first-time-ever">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1607950396-1_4.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1607950396-1_4.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/beats/article/7220/mariah-carey-s-all-i-want-for-christmas-is-you-tops-the-uk-chart-for-the-first-time-ever">Mariah
                                                                    Carey’s “All I Want For Christmas Is You” Tops UK
                                                                    Official Charts For The First Time Ever</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 14, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/beats/article/7204/taylor-swift-drops-new-suprise-album-evermore-during-weekend">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1607847046-1.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1607847046-1.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/beats/article/7204/taylor-swift-drops-new-suprise-album-evermore-during-weekend">Taylor
                                                                    Swift Drops Surprise Album “Evermore” Over The
                                                                    Weekend, Her 2nd Full-Length Release In 2020</a>
                                                                </h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 13, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/beats/article/4669/check-out-this-week-s-hottest-tracks-on-nilefm-s-hit-30-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1578573858-hit30_egypt_website.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1578573858-hit30_egypt_website.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/beats/article/4669/check-out-this-week-s-hottest-tracks-on-nilefm-s-hit-30-">This
                                                                    Week's Hottest Tracks On NileFM's "Hit 30 Egypt"</a>
                                                                </h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 10, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline music"><a
                                                href="/entertainment"><span>Entertainment</span></a><strong><a
                                                href="/entertainment">View More</a></strong></h2>
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                    <div className="no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/entertainment/article/7241/lily-james-sebastian-stan-to-play-pamela-anderson-tommy-lee-in-upcoming-series">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608121750-1_4.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608121750-1_4.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/entertainment/article/7241/lily-james-sebastian-stan-to-play-pamela-anderson-tommy-lee-in-upcoming-series">Lily
                                                                        James &amp; Sebastian Stan To Play Pamela
                                                                        Anderson &amp; Ex-Husband Tommy Lee In Upcoming
                                                                        Series</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>Actors Lily James and Sebastian Stan are set to star in an upcoming series about one of the most iconic celebrity marriages Hollywood has ever seen, that of former… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/entertainment/article/7239/biopic-detailing-paris-hilton-lindsey-lohan-s-infamous-fight-in-development">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608120231-BeFunky-collage_66.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608120231-BeFunky-collage_66.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/entertainment/article/7239/biopic-detailing-paris-hilton-lindsey-lohan-s-infamous-fight-in-development">Biopic
                                                                        Detailing Paris Hilton &amp; Lindsey Lohan's
                                                                        Infamous Fight In Development</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>The party lives of Paris Hilton and Lindsey Lohan are already in the books of pop culture history, and their much-publicized conflict captured the world's attention as… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/entertainment/article/7238/lana-del-rey-engaged-to-fellow-musician-clayton-johnson">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608118264-1_3.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608118264-1_3.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/entertainment/article/7238/lana-del-rey-engaged-to-fellow-musician-clayton-johnson">Lana
                                                                    Del Rey Engaged To Fellow Musician Clayton
                                                                    Johnson</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/entertainment/article/7236/marvel-announced-new-show-legends-that-will-focus-on-each-character-from-the-mcu">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608115541-1_2.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608115541-1_2.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/entertainment/article/7236/marvel-announced-new-show-legends-that-will-focus-on-each-character-from-the-mcu">Marvel
                                                                    Announces New TV Show “Legends” Set To Explore Each
                                                                    MCU Character In Depth</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/entertainment/article/7234/naomi-ackie-to-play-whitney-houston-in-i-wanna-dance-with-somebody-biopic">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608114220-1_1.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608114220-1_1.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/entertainment/article/7234/naomi-ackie-to-play-whitney-houston-in-i-wanna-dance-with-somebody-biopic">"Star
                                                                    Wars" Star Naomi Ackie To Play Whitney Houston In “I
                                                                    Wanna Dance With Somebody” Biopic</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline entertainm"><a
                                                href="/digest"><span>Digest</span></a><strong><a href="/digest">View
                                                More</a></strong></h2>
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                    <div className="no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/digest/article/7235/5-000-year-old-ancient-egyptian-artifact-found-in-a-cigar-tin-at-scottish-university-after-being-lost-for-over-a-century">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608116097-BeFunky-collage_-_2020-12-16T125225.817.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608116097-BeFunky-collage_-_2020-12-16T125225.817.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/digest/article/7235/5-000-year-old-ancient-egyptian-artifact-found-in-a-cigar-tin-at-scottish-university-after-being-lost-for-over-a-century">5,000-Year-Old
                                                                        Ancient Egyptian Artifact Found In A Cigar Tin
                                                                        At Scottish University After Being Lost For Over
                                                                        A Century</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>An Egyptian archaeologist at the University of Aberdeen in Scotland just found a 5,000-year-old ancient Egyptian artifact stashed away in a cigar box. The artifact is one of… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/digest/article/7232/japan-s-twitter-killer-sentenced-to-death-after-killing-dismembering-nine-victims">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608111674-BeFunky-collage_-_2020-12-16T113855.413.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608111674-BeFunky-collage_-_2020-12-16T113855.413.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/digest/article/7232/japan-s-twitter-killer-sentenced-to-death-after-killing-dismembering-nine-victims">Japan's
                                                                        "Twitter Killer" Sentenced To Death After
                                                                        Killing &amp; Dismembering Nine Victims</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>A Japanese man who has come to be known as the “Twitter killer” has been sentenced to death after confessing to murdering and dismembering nine people… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/digest/article/7231/schools-universities-close-in-four-egyptian-governorates-due-to-bad-weather-including-cairo-giza">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608101679-BeFunky-collage_-_2020-12-16T084852.882.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608101679-BeFunky-collage_-_2020-12-16T084852.882.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/digest/article/7231/schools-universities-close-in-four-egyptian-governorates-due-to-bad-weather-including-cairo-giza">Schools &amp; Universities
                                                                    Close In Four Egyptian Governorates Due To Bad
                                                                    Weather, Including Cairo &amp; Giza</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 16, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/digest/article/7221/hurghada-airport-tightens-security-after-tourists-present-fake-covid-19-pcr-tests">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608020451-BeFunky-collage_-_2020-12-15T102049.095.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1608020451-BeFunky-collage_-_2020-12-15T102049.095.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/digest/article/7221/hurghada-airport-tightens-security-after-tourists-present-fake-covid-19-pcr-tests">Hurghada
                                                                    Airport Tightens Security After Tourists Present
                                                                    Fake COVID-19 PCR Tests</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 15, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/digest/article/7219/zodiac-killer-cipher-decrypted-over-50-years-after-it-was-first-received">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1607949276-1_3.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1607949276-1_3.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/digest/article/7219/zodiac-killer-cipher-decrypted-over-50-years-after-it-was-first-received">After
                                                                    51 Years, The Zodiac Killer's Infamous Cipher Has
                                                                    Been Solved</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 14, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline undefined"><a
                                                href="/quizzes"><span>Quizzes</span></a><strong><a href="/quizzes">View
                                                More</a></strong></h2>
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                    <div className="no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/quizzes/article/7137/quiz-how-lonely-are-you-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606982302-BeFunky-collage_47.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1606982302-BeFunky-collage_47.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/quizzes/article/7137/quiz-how-lonely-are-you-">Quiz:
                                                                        How Lonely Are You?</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 3, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}><span></span>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/quizzes/article/6741/quiz-what-tv-anti-hero-are-you-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1602158036-BeFunky-collage_-_2020-10-08T134944.148.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1602158036-BeFunky-collage_-_2020-10-08T134944.148.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/quizzes/article/6741/quiz-what-tv-anti-hero-are-you-">Quiz:
                                                                        What TV Anti-Hero Are You?</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Oct 8, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}><span></span>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/quizzes/article/6582/quiz-how-well-did-you-keep-up-with-the-kardashians-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1600340076-BeFunky-collage_47.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1600340076-BeFunky-collage_47.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/quizzes/article/6582/quiz-how-well-did-you-keep-up-with-the-kardashians-">Quiz:
                                                                    How Well Did You Keep Up With The Kardashians?</a>
                                                                </h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Sep 17, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/quizzes/article/6409/quiz-how-woke-are-you-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1598516696-BeFunky-collage_-_2020-08-27T102221.907.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1598516696-BeFunky-collage_-_2020-08-27T102221.907.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/quizzes/article/6409/quiz-how-woke-are-you-">Quiz:
                                                                    How Woke Are You?</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Aug 27, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/quizzes/article/6326/quiz-which-planet-matches-your-personality-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1597671989-BeFunky-collage_100.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1597671989-BeFunky-collage_100.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/quizzes/article/6326/quiz-which-planet-matches-your-personality-">Quiz:
                                                                    Which Planet Matches Your Personality?</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Aug 17, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline life_style"><a
                                                href="/life"><span>Life</span></a><strong><a href="/life">View More</a></strong>
                                            </h2>
                                                <div
                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_news2 no_padding">
                                                    <div className="article_box">
                                                        <div className="image_box_relative">
                                                            <div className="article_data_relative"><h2
                                                                className="article_title"><a
                                                                href="/life/article/7223/nilefm-nogoum-fm-win-big-at-mena-region-radio-awards-this-year">NileFM &amp; Nogoum
                                                                FM Win Big At Some Of MENA's Biggest Radio Awards This
                                                                Year</a></h2><span className="news_date"><i
                                                                className="fa fa-calendar" aria-hidden="true"></i><span>Dec 15, 2020</span></span>
                                                            </div>
                                                            <a href="/life/article/7223/nilefm-nogoum-fm-win-big-at-mena-region-radio-awards-this-year">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608023763-BeFunky-collage_-_2020-12-15T111245.167.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1608023763-BeFunky-collage_-_2020-12-15T111245.167.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                    </div>
                                                    <div className="article_box">
                                                        <div className="image_box_relative">
                                                            <div className="article_data_relative"><h2
                                                                className="article_title"><a
                                                                href="/life/article/7020/7-body-modifications-way-moreextreme-than-tattooing">7
                                                                Body Modifications That Are Way More Extreme Than
                                                                Tattoos &amp; Piercings </a></h2><span
                                                                className="news_date"><i className="fa fa-calendar"
                                                                                         aria-hidden="true"></i><span>Dec 13, 2020</span></span>
                                                            </div>
                                                            <a href="/life/article/7020/7-body-modifications-way-moreextreme-than-tattooing">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1605614786-BeFunky-collage_-_2020-11-17T135104.482.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1605614786-BeFunky-collage_-_2020-11-17T135104.482.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                    </div>
                                                    <div className="article_box">
                                                        <div className="image_box_relative">
                                                            <div className="article_data_relative"><h2
                                                                className="article_title"><a
                                                                href="/life/article/7135/singapore-approves-world-s-first-kill-free-lab-grown-meat">Singapore
                                                                Approves The World's First "Kill-Free" Meat, Grown In A
                                                                Lab</a></h2><span className="news_date"><i
                                                                className="fa fa-calendar" aria-hidden="true"></i><span>Dec 3, 2020</span></span>
                                                            </div>
                                                            <a href="/life/article/7135/singapore-approves-world-s-first-kill-free-lab-grown-meat">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606915149-BeFunky-collage_46.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1606915149-BeFunky-collage_46.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                    </div>
                                                    <div className="article_box">
                                                        <div className="image_box_relative">
                                                            <div className="article_data_relative"><h2
                                                                className="article_title"><a
                                                                href="/life/article/7113/oxford-finds-video-games-are-good-for-lockdown-stress">Oxford
                                                                University Study Finds That Video Games Are Good For
                                                                Lockdown Stress</a></h2><span className="news_date"><i
                                                                className="fa fa-calendar" aria-hidden="true"></i><span>Dec 3, 2020</span></span>
                                                            </div>
                                                            <a href="/life/article/7113/oxford-finds-video-games-are-good-for-lockdown-stress">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606741605-BeFunky-collage_39.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1606741605-BeFunky-collage_39.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                    </div>
                                                    <div className="article_box">
                                                        <div className="image_box_relative">
                                                            <div className="article_data_relative"><h2
                                                                className="article_title"><a
                                                                href="/life/article/7131/merriam-webster-dictionary-com-s-word-of-the-year-is-unsurprisingly-pandemic-">Merriam-Webster &amp; Dictionary.com's
                                                                Word Of The Year Is Unsurprisingly "Pandemic"</a></h2>
                                                                <span className="news_date"><i
                                                                    className="fa fa-calendar"
                                                                    aria-hidden="true"></i><span>Dec 2, 2020</span></span>
                                                            </div>
                                                            <a href="/life/article/7131/merriam-webster-dictionary-com-s-word-of-the-year-is-unsurprisingly-pandemic-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606907645-BeFunky-collage_-_2020-12-02T131006.803.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1606907645-BeFunky-collage_-_2020-12-02T131006.803.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="home_news">
                                            <div className="row"><h2 className="headline technology"><a href="/geekdom"><span>Geekdom</span></a><strong><a
                                                href="/geekdom">View More</a></strong></h2>
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                    <div className="no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/geekdom/article/7192/chris-hemsworth-discusses-thor-love-thunder-work-out-training-regime">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1607517340-BeFunky-collage_61.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1607517340-BeFunky-collage_61.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/geekdom/article/7192/chris-hemsworth-discusses-thor-love-thunder-work-out-training-regime">Chris
                                                                        Hemsworth Reveals His "Thor: Love &amp; Thunder"
                                                                        Workout Regime</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 9, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>Chris Hemsworth has released a number of videos, interviews, and workout programs that are inspired by his training for the upcoming Marvel flick "Thor: Love &amp;… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/geekdom/article/7119/super-nintendo-world-theme-park-opening-date-finally-announced">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606818943-1_3.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1606818943-1_3.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/geekdom/article/7119/super-nintendo-world-theme-park-opening-date-finally-announced">Brand
                                                                        New Super Nintendo World Theme Park Is Every 90s
                                                                        Kid's Dream Come True</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Dec 1, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>The long-awaited Super Nintendo World Theme Park in Universal Studios Japan has finally gotten an opening date and a brand new trailer and it looks nothing short of… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/7071/twitter-to-warn-users-about-liking-about-tweets-with-disputed-information">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606223311-1_5.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1606223311-1_5.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/7071/twitter-to-warn-users-about-liking-about-tweets-with-disputed-information">Twitter
                                                                    To Warn Users About Liking Tweets With Disputed
                                                                    Information With New Feature</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 24, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/7068/snapchat-introduces-new-feature-spotlight-to-compete-with-tiktok">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606219902-1_3.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1606219902-1_3.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/7068/snapchat-introduces-new-feature-spotlight-to-compete-with-tiktok">Snapchat
                                                                    Introduces New Feature “Spotlight” To Compete With
                                                                    TikTok</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 24, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/6990/google-states-it-may-start-deleting-your-files-on-its-platform-if-you-re-inactive-for-too-long">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1605181109-1._5.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1605181109-1._5.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/6990/google-states-it-may-start-deleting-your-files-on-its-platform-if-you-re-inactive-for-too-long">Google
                                                                    Announces It “May” Start Deleting Your Files If
                                                                    You’re Inactive For Too Long</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 19, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/geekdom/article/6970/playstation-5-designer-reveals-the-console-was-supposed-to-be-much-larger-">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1605010197-1_4.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1605010197-1_4.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/geekdom/article/6970/playstation-5-designer-reveals-the-console-was-supposed-to-be-much-larger-">PlayStation
                                                                        5 Designer Reveals The Console Was Supposed To
                                                                        Be “Much Larger”</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 10, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>If you’ve looked at the PlayStation 5 in videos over the internet, you know that it’s a huge device standing taller than its current competitor and even bigger than previous… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                            <div className="article_box row">
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <a href="/geekdom/article/6936/-pokémon-go-brings-in-4-2-billion-globally-making-2020-its-best-year-yet">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1604495892-1_4.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1604495892-1_4.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div
                                                                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                                                    <h2 className="article_title"><a
                                                                        href="/geekdom/article/6936/-pokémon-go-brings-in-4-2-billion-globally-making-2020-its-best-year-yet">“Pokémon
                                                                        Go” Has Its Best Year Yet, Bringing In Over $1
                                                                        Billion Amid The Pandemic</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 4, 2020</span></span>
                                                                        </div>
                                                                        <div className="col-sm-6 col-xs-6"></div>
                                                                    </div>
                                                                    <p className="article_desc">
                                                                        <div style={{overflow: 'hidden'}}>
                                                                            <div><span>2020 has been a hard time for everybody, and with more people staying at home, everyone has been looking for ways to entertain themselves, and it appears that a lot of… </span>
                                                                            </div>
                                                                        </div>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/6925/researchers-develop-ai-that-can-identify-asymptomatic-people-with-covid-19-via-their-coughs">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1604406009-1_5.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1604406009-1_5.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/6925/researchers-develop-ai-that-can-identify-asymptomatic-people-with-covid-19-via-their-coughs">Researchers
                                                                    Develop A.I. That Can Identify Asymptomatic COVID-19
                                                                    Patients Just By Their Cough</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Nov 3, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/6806/nasa-grants-telecommunications-company-to-create-4g-network-on-the-moon">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1603019417-1_2.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1603019417-1_2.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/6806/nasa-grants-telecommunications-company-to-create-4g-network-on-the-moon">NASA
                                                                    Grants Telecommunications Company To Create 4G
                                                                    Network On The Moon</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Oct 22, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="article_box small-article_box row">
                                                                <div
                                                                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                    <a href="/geekdom/article/6756/tiktok-partners-with-john-lennon-s-estate-to-bring-singer-s-music-to-creators-to-make-videos-with">
                                                                        <div className="article_img">
                                                                            <div
                                                                                style={{width: '100%' ,height: '100%' ,backgroundImage:'url(https://storage.googleapis.com/nrpassets/uploads/articles/1602494061-1.jpg)', backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                            <img
                                                                                src="https://storage.googleapis.com/nrpassets/uploads/articles/1602494061-1.jpg"
                                                                                className="for-s-mobile" alt=""/></div>
                                                                    </a></div>
                                                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                    className="article_title"><a
                                                                    href="/geekdom/article/6756/tiktok-partners-with-john-lennon-s-estate-to-bring-singer-s-music-to-creators-to-make-videos-with">TikTok
                                                                    Partners With John Lennon’s Estate To Bring Singer’s
                                                                    Music To Video-Making Creatives</a></h2>
                                                                    <div className="row">
                                                                        <div className="col-sm-6 col-xs-6 no_padding">
                                                                            <span className="news_date"><i
                                                                                className="fa fa-calendar"
                                                                                aria-hidden="true"></i><span>Oct 12, 2020</span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>                                <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div className="side_bar">
                                        <section className="side_img"></section>
                                        <section className="side_news">
                                            <div className="react-tabs" data-tabs="true">
                                                <ul className="react-tabs__tab-list" role="tablist">
                                                    <li className="react-tabs__tab react-tabs__tab--selected" role="tab"
                                                        id="react-tabs-28572" aria-selected="true" aria-disabled="false"
                                                        aria-controls="react-tabs-28573" tabIndex="0">Trending
                                                    </li>
                                                    <li className="react-tabs__tab" role="tab" id="react-tabs-28574"
                                                        aria-selected="false" aria-disabled="false"
                                                        aria-controls="react-tabs-28575">Latest
                                                    </li>
                                                </ul>
                                                <div className="react-tabs__tab-panel react-tabs__tab-panel--selected"
                                                     role="tabpanel" id="react-tabs-28573"
                                                     aria-labelledby="react-tabs-28572">
                                                    <div className="col-lg-12 col-xs-12 no_padding">
                                                        <div className="article_box small-article_box no_padding row">
                                                            <div
                                                                className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                <a href="/entertainment/article/5130/12-celebrities-who-deeply-regretted-doing-nude-scenes-why">
                                                                    <div className="article_img">
                                                                        <div
                                                                            style={{
                                                                                width: '100%',
                                                                                height: '100%',
                                                                                backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1583747716-BeFunky-collage_43.jpg)',
                                                                                backgroundRepeat: 'no-repeat',
                                                                                backgroundPosition: 'center center',
                                                                                backgroundSize: 'cover'
                                                                            }}></div>
                                                                        < img
                                                                            src="https://storage.googleapis.com/nrpassets/uploads/articles/1583747716-BeFunky-collage_43.jpg"
                                                                            className="for-s-mobile" alt=""/></div>
                                                                </a></div>
                                                            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                className="article_title"><a
                                                                href="/entertainment/article/5130/12-celebrities-who-deeply-regretted-doing-nude-scenes-why">12
                                                                Celebrities Who Deeply Regretted Doing Nude
                                                                Scenes &amp; Why</a></h2>
                                                                <div className="row">
                                                                    <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                        className="news_date"><i
                                                                        className="fa fa-calendar"
                                                                        aria-hidden="true"></i><span>Mar 9, 2020</span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 col-xs-12 no_padding">
                                                        <div className="article_box small-article_box no_padding row">
                                                            <div
                                                                className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                <a href="/entertainment/article/1260/7-hunky-hollywood-men-you-didn-t-know-had-plastic-surgery">
                                                                    <div className="article_img">
                                                                        <div
                                                                            style={{
                                                                                width: '100%',
                                                                                height: '100%',
                                                                                backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1523976507-tom-cruise-plastic-surgery1.jpg)',
                                                                                backgroundRepeat: 'no-repeat',
                                                                                backgroundPosition: 'center center',
                                                                                backgroundSize: 'cover'
                                                                            }}></div>
                                                                        <img
                                                                            src="https://storage.googleapis.com/nrpassets/uploads/articles/1523976507-tom-cruise-plastic-surgery1.jpg"
                                                                            className="for-s-mobile" alt=""/></div>
                                                                </a></div>
                                                            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                className="article_title"><a
                                                                href="/entertainment/article/1260/7-hunky-hollywood-men-you-didn-t-know-had-plastic-surgery">7
                                                                Hunky Hollywood Men You Didn’t Know Had Plastic
                                                                Surgery</a></h2>
                                                                <div className="row">
                                                                    <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                        className="news_date"><i
                                                                        className="fa fa-calendar"
                                                                        aria-hidden="true"></i><span>Apr 17, 2018</span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 col-xs-12 no_padding">
                                                        <div className="article_box small-article_box no_padding row">
                                                            <div
                                                                className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                                <a href="/entertainment/article/7283/gal-gadot-responds-to-whitewashing-criticism-over-cleopatra-portrayal">
                                                                    <div className="article_img">
                                                                        <div
                                                                            style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608720497-BeFunky-collage_-_2020-12-23T124618.027.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                        <img
                                                                            src="https://storage.googleapis.com/nrpassets/uploads/articles/1608720497-BeFunky-collage_-_2020-12-23T124618.027.jpg"
                                                                            className="for-s-mobile" alt=""/></div>
                                                                </a></div>
                                                            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                                className="article_title"><a
                                                                href="/entertainment/article/7283/gal-gadot-responds-to-whitewashing-criticism-over-cleopatra-portrayal">Gal
                                                                Gadot Responds To Whitewashing Criticism Over Cleopatra
                                                                Portrayal</a></h2>
                                                                <div className="row">
                                                                    <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                        className="news_date"><i
                                                                        className="fa fa-calendar"
                                                                        aria-hidden="true"></i><span>Dec 23, 2020</span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="react-tabs__tab-panel" role="tabpanel"
                                                     id="react-tabs-28575" aria-labelledby="react-tabs-28574"></div>
                                            </div>
                                        </section>
                                        <section className="side_news sideNewsThree">
                                            <div><h4>Celebrity</h4>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7318/jennifer-aniston-criticized-for-pandemic-christmas-ornament-fans-defend-her-online">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609148885-1_4.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609148885-1_4.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7318/jennifer-aniston-criticized-for-pandemic-christmas-ornament-fans-defend-her-online">Jennifer
                                                            Aniston Slammed For Her COVID-Themed Christmas
                                                            Decorations</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 28, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7307/anwar-hadid-says-he-will-not-get-covid-19-vaccine-because-its-unnatural">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609071802-BeFunky-collage_-_2020-12-27T141658.143.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609071802-BeFunky-collage_-_2020-12-27T141658.143.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7307/anwar-hadid-says-he-will-not-get-covid-19-vaccine-because-its-unnatural">Anwar
                                                            Hadid Says He Will Not Get COVID-19 Vaccine Because It's
                                                            Unnatural</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 27, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7306/michael-jackson-neverland-ranch-sold-to-billionaire">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609070168-1_3.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609070168-1_3.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7306/michael-jackson-neverland-ranch-sold-to-billionaire">Michael
                                                            Jackson's Neverland Ranch Sold To U.S. Billionaire</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 27, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7304/kevin-spacey-returns-subverting-expectations-and-calls-in-support-of-suicide-prevention">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609063799-1_1.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609063799-1_1.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7304/kevin-spacey-returns-subverting-expectations-and-calls-in-support-of-suicide-prevention">Kevin
                                                            Spacey Returns For Another Christmas Video &amp; Calls For
                                                            Support Of Suicide Prevention</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 27, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7302/celebrities-celebrate-christmas-2020-their-own-way">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609059625-1.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609059625-1.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7302/celebrities-celebrate-christmas-2020-their-own-way">Celebrities
                                                            Celebrate Christmas 2020 Their Own Way</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 27, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div><h4>Arts &amp; Culture</h4>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7314/royal-mummies-parade-delayed-to-2021-due-to-covid-19">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1609143946-1_1.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1609143946-1_1.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7314/royal-mummies-parade-delayed-to-2021-due-to-covid-19">Royal
                                                            Mummies Parade Delayed To 2021 Due To COVID-19</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 28, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7230/10-weirdest-christmas-traditions-from-around-the-world">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608038089-1_6.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1608038089-1_6.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7230/10-weirdest-christmas-traditions-from-around-the-world">10
                                                            Weirdest Christmas Traditions From Around The World</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 24, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7280/egypt-to-inaugurate-new-capital-musuem-in-early-2021">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608716107-BeFunky-collage_-_2020-12-23T113418.798.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1608716107-BeFunky-collage_-_2020-12-23T113418.798.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7280/egypt-to-inaugurate-new-capital-musuem-in-early-2021">Egypt
                                                            To Inaugurate New Capital Museum In Early 2021</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 23, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7278/meet-fadi-francis-an-egyptian-artist-who-makes-ceramic-statues-of-international-celebrities">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608709310-BeFunky-collage_-_2020-12-23T093842.753.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1608709310-BeFunky-collage_-_2020-12-23T093842.753.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7278/meet-fadi-francis-an-egyptian-artist-who-makes-ceramic-statues-of-international-celebrities">An
                                                            Egyptian Artist Makes Ceramic Statues Of International
                                                            Celebrities Including Queen Elizabeth II &amp; Pope
                                                            Francis</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 23, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7263/egypt-oud-schools-markets-experience-unprecedented-interest-during-covid-19">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1608547811-BeFunky-collage_76.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1608547811-BeFunky-collage_76.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7263/egypt-oud-schools-markets-experience-unprecedented-interest-during-covid-19">Egypt
                                                            Oud Schools &amp; Markets Experience "Unprecedented
                                                            Interest" During COVID-19</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 21, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div><h4>Quizzes</h4>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/7137/quiz-how-lonely-are-you-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1606982302-BeFunky-collage_47.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1606982302-BeFunky-collage_47.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/7137/quiz-how-lonely-are-you-">Quiz:
                                                            How Lonely Are You?</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Dec 3, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/6741/quiz-what-tv-anti-hero-are-you-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1602158036-BeFunky-collage_-_2020-10-08T134944.148.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1602158036-BeFunky-collage_-_2020-10-08T134944.148.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/6741/quiz-what-tv-anti-hero-are-you-">Quiz:
                                                            What TV Anti-Hero Are You?</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Oct 8, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/6669/quiz-who-are-you-at-the-office-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1601388092-BeFunky-collage_-_2020-09-29T155855.024.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1601388092-BeFunky-collage_-_2020-09-29T155855.024.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/6669/quiz-who-are-you-at-the-office-">Quiz:
                                                            Who Are You At The Office?</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Sep 29, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/6582/quiz-how-well-did-you-keep-up-with-the-kardashians-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1600340076-BeFunky-collage_47.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1600340076-BeFunky-collage_47.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/6582/quiz-how-well-did-you-keep-up-with-the-kardashians-">Quiz:
                                                            How Well Did You Keep Up With The Kardashians?</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Sep 17, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-xs-12 no_padding">
                                                    <div className="article_box small-article_box no_padding row">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding">
                                                            <a href="/undefined/article/6517/quiz-how-well-do-you-know-your-music-history-">
                                                                <div className="article_img">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/articles/1599726165-BeFunky-collage_-_2020-09-10T100239.844.jpg)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                    <img
                                                                        src="https://storage.googleapis.com/nrpassets/uploads/articles/1599726165-BeFunky-collage_-_2020-09-10T100239.844.jpg"
                                                                        className="for-s-mobile" alt=""/></div>
                                                            </a></div>
                                                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h2
                                                            className="article_title"><a
                                                            href="/undefined/article/6517/quiz-how-well-do-you-know-your-music-history-">Quiz:
                                                            How Well Do You Know Your Music History?</a></h2>
                                                            <div className="row">
                                                                <div className="col-sm-6 col-xs-6 no_padding"><span
                                                                    className="news_date"><i className="fa fa-calendar"
                                                                                             aria-hidden="true"></i><span>Sep 10, 2020</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="side_schedule"><h2 className="headline">
                                            <span> Schedule </span></h2>
                                            <ul>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-lg-3 col-xs-3 no_padding">
                                                            <div className="show_img">
                                                                <div className="LazyLoad is-visible is-loaded"><img
                                                                    src="https://storage.googleapis.com/nrpassets/uploads/shows/1590912446-Good_Vibes_Only.jpg"
                                                                    alt="/static/img_loader.jpg" className="className"/>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-9 col-xs-9">
                                                            <div className="row1"><span
                                                                className="schedule_status"><span> Now   </span><span> Next  </span><span> Later </span></span>
                                                                <div className="presenter_image">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/presenters/1578217463-a1.png)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                </div>
                                                            </div>
                                                            <h2 className="article_title"><a
                                                                href="/show/46/good-vibes-only">
                                                                <div style={{overflow: 'hidden'}}>
                                                                    <span>Good Vibes Only</span></div>
                                                            </a></h2>
                                                            <div className="row1"><h5>With: &nbsp;<a
                                                                href="/presenter/25/zeinab">Zeinab Hegarty</a></h5><span
                                                                className="show_time"><i className="fa fa-clock-o"
                                                                                         aria-hidden="true"></i> &nbsp;01:00 PM - 03:00 PM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-lg-3 col-xs-3 no_padding">
                                                            <div className="show_img">
                                                                <div className="LazyLoad is-visible is-loaded"><img
                                                                    src="https://storage.googleapis.com/nrpassets/uploads/shows/1578479140-Mixalicious_website.jpg"
                                                                    alt="/static/img_loader.jpg" className="className"/>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-9 col-xs-9">
                                                            <div className="row1"><span
                                                                className="schedule_status"><span> Now   </span><span> Next  </span><span> Later </span></span>
                                                                <div className="presenter_image">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(&quot;)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                </div>
                                                            </div>
                                                            <h2 className="article_title"><a
                                                                href="/show/49/mixalicious">
                                                                <div style={{overflow: 'hidde'}}><span>Mixalicious</span>
                                                                </div>
                                                            </a></h2>
                                                            <div className="row1"><span className="show_time"><i
                                                                className="fa fa-clock-o"
                                                                aria-hidden="true"></i> &nbsp;03:00 PM - 04:00 PM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-lg-3 col-xs-3 no_padding">
                                                            <div className="show_img">
                                                                <div className="LazyLoad is-visible is-loaded"><img
                                                                    src="https://storage.googleapis.com/nrpassets/uploads/shows/1590913422-ND_website_copy.jpg"
                                                                    alt="/static/img_loader.jpg" className="className"/>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-9 col-xs-9">
                                                            <div className="row1"><span
                                                                className="schedule_status"><span> Now   </span><span> Next  </span><span> Later </span></span>
                                                                <div className="presenter_image">
                                                                    <div
                                                                        style={{width: '100%', height: '100%', backgroundImage: 'url(https://storage.googleapis.com/nrpassets/uploads/presenters/1578217408-a13.png)',backgroundRepeat: 'no-repeat', backgroundPosition: 'center center', backgroundSize: 'cover'}}></div>
                                                                </div>
                                                            </div>
                                                            <h2 className="article_title"><a
                                                                href="/show/20/nilefm-drive">
                                                                <div style={{overflow: 'hidden'}}><span>NileFM Drive</span>
                                                                </div>
                                                            </a></h2>
                                                            <div className="row1"><h5>With: &nbsp;<a
                                                                href="/presenter/22/mark-somers">Mark Somers</a></h5>
                                                                <span className="show_time"><i className="fa fa-clock-o"
                                                                                               aria-hidden="true"></i> &nbsp;04:00 PM - 07:00 PM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </section>
                                        <section className="side_img"></section>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <section className="home_videos videos">
                                        <div className="row"><h2 className="headline "><a
                                            href="videos.html"><span>Videos</span></a><strong><a href="videos.html">View
                                            More</a></strong></h2>
                                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_out">
                                                <div className="row">
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/Aif2L_6Yzzw/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/157/exclusive-interview-with-jade-thirlwall">
                                                            <div style={{overflow: 'hidden'}}><span>Exclusive Interview With Jade Thirlwall</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/K_ct_glnlj4/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/158/exclusive-interview-with-felukah">
                                                            <div style={{overflow: 'hidden'}}><span>Exclusive Interview With Felukah</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/zqRoHo5qcbA/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/156/anne-marie-talks-about-her-new-album">
                                                            <div style={{overflow: 'hidden'}}><span>Anne-Marie Talks About Her New Album</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/AOAPJi2FYds/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/151/lisa-vicari-dark-season-3-exclusive-interview">
                                                            <div style={{overflow: 'hidden'}}><span>Lisa Vicari - Dark Season 3 Exclusive Interview</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/09WVCj26mf8/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/152/louis-hofmann-dark-season-3-exclusive-interview">
                                                            <div style={{overflow: 'hidden'}}><span>Louis Hofmann - Dark Season 3 Exclusive Interview</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/2JKl17FhLwo/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/150/interview-with-asser-yassin">
                                                            <div style={{overflow: 'hidden'}}><span>Interview With Asser Yassin</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/sfT3d3eVzdQ/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/148/sally-interviews-the-ayoub-sisters">
                                                            <div style={{overflow: 'hidden'}}><span>Sally Interviews The Ayoub Sisters</span>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 video_box">
                                                        <div className="video_img">
                                                            <div className="LazyLoad is-visible is-loaded"><img
                                                                src="https://img.youtube.com/vi/PL6406jH-KA/mqdefault.jpg"
                                                                alt="/static/img_loader_video.jpg"
                                                                className="className"/>
                                                            </div>
                                                        </div>
                                                        <h2 className="article_title"><a
                                                            href="/video/146/la-casa-de-papel-money-heist-interview-w-alba-flores-amp-darko-peric">
                                                            <div style={{overflow: 'hidden'}}>
                                                                <div><span>La Casa De Papel (Money Heist) Interview w/ Alba Flores… </span>
                                                                </div>
                                                            </div>
                                                        </a></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer style={{textAlign: 'left'}}>
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer_links">
                                <div className="row">
                                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-lg"><h3> Explore </h3>
                                        <ul>
                                            <li><a href="/geekdom"> Geekdom </a></li>
                                            <li><a href="/beats"> Beats </a></li>
                                            <li><a href="/life"> Life </a></li>
                                            <li><a href="/videos"> Videos </a></li>
                                            <li><a href="/digest"> Digest </a></li>
                                            <li><a href="/entertainment"> Entertainment </a></li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-lg"><h3> Information </h3>
                                        <ul>
                                            <li><a href="/contact-us"> Contact Us </a></li>
                                            <li><a href="/advertise"> Advertise </a></li>
                                            <li><a href="/about"> About Us </a></li>
                                            <li><a href="/privacy"> Privacy </a></li>
                                            <li><a href="/terms-of-use"> Terms of Use </a></li>
                                            <li><a href="/copyrights-policy"> Copyrights Policy </a></li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-lg"><h3> Information </h3>
                                        <ul>
                                            <li><a href="/rules"> Competition Rules </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="row">
                                    <div style={{overflowY: 'scroll', height: '230px'}}
                                         className="col-lg-6 col-md-6 col-sm-12 col-xs-12 twitter">
                                        <div>
                                            <iframe id="twitter-widget-0" scrolling="no" frameBorder="0"
                                                    allowTransparency="true" allowFullScreen="true"
                                                    className="twitter-timeline twitter-timeline-rendered"
                                                    style={{
                                                        position: 'static',
                                                        visibility: 'visible',
                                                        display: 'inline-block',
                                                        width: '100%',
                                                        height: '658.625px',
                                                        padding: '0px',
                                                        border: 'none',
                                                        maxWidth: '100%',
                                                        minWidth: '180px',
                                                        marginTop: '0px',
                                                        marginBottom: '0px',
                                                    }}
                                                    data-widget-id="profile:NileFM" title="Twitter Timeline"></iframe>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer_social"><img
                                        src="/static/logo.png" alt="NileFM Logo"/>
                                        <ul>
                                            <li><a href="https://www.facebook.com/NileFM" target="_blank"><img
                                                src="/static/facebook.svg" alt=""/></a></li>
                                            <li><a href="https://twitter.com/NileFM" target="_blank"><img
                                                src="/static/twitter.svg" alt=""/></a></li>
                                            <li><a href="https://www.instagram.com/nilefmonline/" target="_blank"><img
                                                src="/static/instagram.svg" alt=""/></a></li>
                                            <li><a href="https://www.youtube.com/user/NileFM" target="_blank"><img
                                                src="/static/youtube.svg" alt=""/></a></li>
                                            <li><a href="https://www.mixcloud.com/nilefm/" target="_blank"><img
                                                src="/static/mix-cloud.png" alt=""/></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>

    );
    }
    }